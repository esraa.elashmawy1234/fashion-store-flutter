// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:flutter_fashion_store_application_/styles/colorstyles.dart';

// class searchscreen extends StatefulWidget {
//   const searchscreen({Key? key}) : super(key: key);

//   @override
//   State<searchscreen> createState() => _searchState();
// }

// class _searchState extends State<searchscreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: appColor,
//       appBar: AppBar(
//         backgroundColor: appColor,
//         elevation: 0,
//         title: const Text("Women Clothes"),
//         actions: [
//           Align(
//               alignment: Alignment.topLeft,
//               child: IconButton(
//                 onPressed: () {
//                   // method to show the search bar
//                   showSearch(
//                       context: context,
//                       // delegate to customize the search bar
//                       delegate: CustomSearchDelegate());
//                 },
//                 icon: const Icon(Icons.search, color: Colors.white,),
//               )),
//         ],
//       ),
//     );
//   }
// }

class CustomSearchDelegate extends SearchDelegate {
// Demo list to show querying
  List<String> sugesstions = [
    "Tops",
    "Shirts and blouses",
    "Cardigan and Sweater",
    "Blazers",
    "Pants",
    "Jeans",
    "Shorts",
    "Skirts",
    "Dresses"
  ];
  List<String> recent = ["Dresses", "Skirts", "Blazers", "Pants"];
// first overwrite to
// clear the search text
  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
      textTheme: const TextTheme(
        // Use this to change the query's text style
        headline6: TextStyle(fontSize: 15.0, color: Colors.white),
      ),
      inputDecorationTheme: InputDecorationTheme(
          filled: true,
          fillColor: const Color.fromARGB(255, 44, 43, 43),
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(40.0),
          )),
      appBarTheme: const AppBarTheme(
        backgroundColor: appColor,
      ),
    );
  }

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      // IconButton(
      //   alignment: Alignment.topLeft,
      //   onPressed: () {
      //     query = '';
      //   },
      //   icon: const Icon(Icons.clear),
      // ),
    ];
  }

// second overwrite to pop out of search menu
  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: const Icon(Icons.arrow_back),
    );
  }

// third overwrite to show query result
  @override
  Widget buildResults(BuildContext context) {
    List<String> matchQuery = [];
    for (var cloth in sugesstions) {
      if (cloth.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(cloth);
      }
    }
      return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return ListTile(
          title: Text(result),
        );
      },
    );
    
  //   return Container(
  //     color: appColor,
  //     child: ListView.separated(
  //         itemCount: matchQuery.length,
  //         itemBuilder: (context, index) {
  //           var result = matchQuery[index];
  //           return ListTile(
  //             title: Text(
  //               result,
  //               style: const TextStyle(color: Colors.white),
  //             ),
  //           );
  //         },
  //         separatorBuilder: (context, index) {
  //           return const Divider(
  //             color: Colors.white,
  //           );
  //         }
  //         ),
  //   );
   }

// last overwrite to show the
// querying process at the runtime
  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> matchQuery = [];
    matchQuery = query.isEmpty ? recent : sugesstions;

    //  for (var cloth in sugesstions) {
    //   if (cloth.toLowerCase().contains(query.toLowerCase())) {
    //     matchQuery.add(cloth);
    //   }
    //  }
    //   return Container(
    //   color: appColor,
    //   child: ListView.builder(
    //     itemCount: matchQuery.length,
    //     itemBuilder: (context, index) {
    //       var result = matchQuery[index];
    //       return ListTile(
    //         title: Text(result, style: AppTextStyle.style,),
    //       );
    //     },
    //   ),
    // );
    return Container(
      color: appColor,
      child: ListView.separated(
          itemCount: matchQuery.length,
          itemBuilder: (context, index) {
            var result = matchQuery[index];
            return ListTile(
              trailing: query.isEmpty
                  ? const Icon(
                      Icons.close,
                      color: Colors.white,
                    )
                  : null,
              title: Text(
                result,
                style: const TextStyle(color: Colors.white),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const Divider(
              color: Colors.white,
            );
          }),
    );
  }
}
