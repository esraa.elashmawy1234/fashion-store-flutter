// ignore_for_file: depend_on_referenced_packages, non_constant_identifier_names, file_names

import 'package:flutter/material.dart';
import 'package:flutter_fashion_store_application_/screens/FilterScreen.dart';
import 'package:flutter_fashion_store_application_/screens/Searchscreen.dart';
import 'package:flutter_fashion_store_application_/styles/colorstyles.dart';

import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../widgets/ScrollOptionsWidget.dart';

class ViewProductsScreen extends StatefulWidget {
  const ViewProductsScreen({Key? key}) : super(key: key);

  @override
  State<ViewProductsScreen> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<ViewProductsScreen> {
    bool pressedpopular=false;
    bool pressednew=false;
    bool pressedprice=false;
    bool pressedprice2=false;
    bool pressedreview=false;
  String sortby='Price: lowest to high';
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    //print(w);
    return Scaffold(
      backgroundColor: appColor,
      appBar: AppBar(
        elevation: 0,
        leading: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          // we don't use only(left: 15) since it will not work in all languages
          child: Icon(
            Icons.arrow_back_ios,
          ),
        ),
        title: const Center(
            child: Text("Women's tops",
                style: TextStyle(
                    fontFamily: 'Metropolis',
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w400))),
        backgroundColor: appColor,
        actions:  [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: IconButton(
              onPressed: () { 
                //  Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => const searchscreen()),
                // );
                 showSearch(
                      context: context,
                      // delegate to customize the search bar
                      delegate: CustomSearchDelegate());
                
                 },
             icon: const Icon( Icons.search),
           
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
          child: Column(
            children: [
              ScrollOptionsWidget(w:w),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: filteroptions(w),
              ),
              griditems(h, w),
              //GriditemsWidget(),
            ],
          )),
    );
  }

  List<Map<String, Object>> values = [
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt SPANISH",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Dorothy Perkins",
      "productname": "Blouse",
      "price": 21,
      "fav": false,
      "discount": 20,
      "after_discount": 14,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Dorothy Perkins",
      "productname": "Blouse",
      "price": 16,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "Shirt",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "Light Blouse",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 16,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 13,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 17,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
  ];


  Widget filteroptions(double w) {
    return SizedBox(
      width: w,
      height: 45,
      // child: Row(
      //   children: [
      //   IconButton(onPressed: (){}, icon: const Icon(Icons.filter_list, color: Colors.white,)),
      //   Text("Filters", style: AppTextStyle.style,),

      //    IconButton(onPressed: (){}, icon: const Icon(Icons.swap_vert, color: Colors.white,)),
      //   Text("Price: lowest to high", style: AppTextStyle.style,),

      //   IconButton(onPressed: (){}, icon: const Icon(Icons.view_list, color: Colors.white,)),

      // ],),
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
        dense: true,
        leading: SizedBox(
          width: w * 0.3,
          child: Row(children: [
            IconButton(
                onPressed: () { Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => const FilterScreen()),
                 );},
                icon: const Icon(
                  Icons.filter_list,
                  color: Colors.white,
                )),
            Text(
              "Filters",
              style: AppbarTextStyle.style,
            ),
          ]),
        ),
        title: SizedBox(
          width: w * 0.3,
          child: Row(children: [
            IconButton(
                onPressed: () {
                  //BottomBarWidget();
                  showMenu();
                },
                icon: const Icon(
                  Icons.swap_vert,
                  color: Colors.white,
                )),
            Text( 
              sortby,
              style: AppbarTextStyle.style,
            ),
          ]),
        ),
        trailing: IconButton(
            onPressed: () {
              
            },
            icon: const Icon(
              Icons.view_list,
              color: Colors.white,
            )),
      ),
    );
  }

  List myProducts = [
    'assets/images/product2.png',
    'assets/images/product1.png',
    'assets/images/product2.png',
    'assets/images/product1.png',
    'assets/images/product2.png',
    'assets/images/product1.png',
    'assets/images/product2.png',
    'assets/images/product1.png',
    'assets/images/product2.png',
    'assets/images/product1.png',
    'assets/images/product2.png',
    'assets/images/product1.png'
  ];

  Widget griditems(double h, double w) {
    return SizedBox(
      height: h * 0.7,
      child: GridView.builder(
          primary: false,
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 300,
              childAspectRatio:1,
              crossAxisSpacing: 1,
              mainAxisSpacing: 60),
          itemCount: values.length,
          itemBuilder: (BuildContext ctx, index) {
            return Stack(
              children: [
                Wrap(
                  children: [
                    Center(
                      child: Stack(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image.asset(
                                '${values[index]['imageurl']}',
                                fit: BoxFit.cover,
                             width: 200,
                              )),
                        ],
                      ),
                    ),
                    product_details(index)
                  ],
                ),
                Positioned(
                  right: 35,
                  top: 205,
                  child: CircleAvatar(
                    backgroundColor: const Color(0xFF2A2C36),
                    maxRadius: 18,
                    child: IconButton(
                      icon: values[index]['fav'] == false
                          ? const Icon(
                              Icons.favorite_outline,
                              color: Color(0xFFDADADA),
                              size: 15,
                            )
                          : const Icon(
                              Icons.favorite,
                              color: secondaryColor,
                              size: 15,
                            ),
                      onPressed: () {
                        setState(() {
                          if (values[index]['fav'] == false) {
                            values[index]['fav'] = true;
                          } else {
                            values[index]['fav'] = false;
                          }
                          // values[index]['fav']= !values[index]['fav'];
                        });
                      },
                    ),
                  ),
                ),
                values[index]['discount'] ==Null? Container():
                Positioned(
                  right: 180,
                  top: 9,
                  child: Chip(
                    padding: const EdgeInsets.all(2),
                    backgroundColor: secondaryColor,
                    label: Text(
                      '-${values[index]['discount']}%',
                      style: AppbarTextStyle.style,
                    ), //Text
                  ),
                )
              ],
            );
          }),
    );
  }

  Widget rating(int currentindex) {
    return Row(
      children: [
        RatingBar.builder(
          itemSize: 13,
          initialRating: 0,
          minRating: 0,
          direction: Axis.horizontal,
          unratedColor: const Color.fromARGB(59, 255, 255, 255),
          //allowHalfRating: true,
          itemCount: 5,
          itemPadding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2),
          itemBuilder: (context, index) {
            return const Icon(
              Icons.star,
              color: Colors.amber,
            );
          },
          onRatingUpdate: (rating) {
            setState(() {
              values[currentindex]['rating'] = rating;
            });
          },
        ),
        Text(
          "(${values[currentindex]['rating']})",
          style: AppbarTextStyle.style,
        )
      ],
    );
  }

  Widget product_details(int index) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 2),
          child: rating(index),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 2),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "${values[index]['brandname']}",
                style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFFABB4BD)),
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 2),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "${values[index]['productname']}",
                style: AppTextStyle.style,
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 42),
          child: Align(
              alignment: Alignment.centerLeft,
              child:  values[index]['discount'] ==Null? 
              Text(
                "${values[index]['price']}\$",
                style: App14TextStyle.style,
              ): Row(
                children: [
                  Text(
                    "${values[index]['price']}\$ ",
                    style:const TextStyle(decoration: TextDecoration.lineThrough,fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14),
                  ),
                 const SizedBox(width: 3,),
                  Text(
                    "${values[index]['after_discount']}\$ ",
                    style:const TextStyle(fontWeight: FontWeight.w400, color: secondaryColor, fontSize: 14),
                  ),
                ],
              )
              ),
        ),
      ],
    );
  }


  showMenu() {
    // bool pressedpopular=false;
    // bool pressednew=false;
    // bool pressedprice=false;
    // bool pressedprice2=false;
    // bool pressedreview=false;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              color: Color(0xFF1E1F28),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                SizedBox(
                  height: 50,
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Image.asset("assets/images/Rectangle.png"),
                      const SizedBox(height: 15),
                      const Center(
                          child: Text("Sort By",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white,
                                  fontSize: 18))),
                    ],
                  ),
                ),
                SizedBox(
                    height: (56 * 4.4).toDouble(),
                    child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(16.0),
                            topRight: Radius.circular(16.0),
                          ),
                          color: appColor,
                        ),
                        child: Stack(
                          alignment: const Alignment(0, 0),
                          // overflow: Overflow.visible,
                          children: <Widget>[
                            ListView(
                              physics: const NeverScrollableScrollPhysics(),
                              children: <Widget>[
                                Container(
                                  color: pressedpopular== true? secondaryColor: appColor,
                                  child: ListTile(
                                    title: const Text(
                                      "Popular",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        pressedpopular=!pressedpopular;
                                        sortby='Popular';
                                      });
                                      print(pressedpopular);
                                      },
                                  ),
                                ),
                                ListTile(
                                  selectedColor: pressednew==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Newest",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {
                                    setState(() {
                                        pressednew!=pressednew;
                                        sortby='Newest';
                                      });
                                  },
                                ),
                                ListTile(
                                  selectedColor: pressedreview==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Customer Review",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {setState(() {
                                      pressedreview=!pressedreview;
                                      sortby='Customer Review';
                                    });},
                                ),
                                ListTile(
                                  selected: true,
                                  selectedColor: pressedprice2==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Price: lowest to high",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {setState(() {
                                      pressedprice=!pressedprice2;
                                      sortby='Price: lowest to high';
                                    });},
                                ),
                                Container(
                                  color: pressedprice? secondaryColor: appColor,
                                  child: ListTile(
                                    title: const Text(
                                      "Price: highest to low",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onTap: () { setState(() {
                                      pressedprice2=!pressedprice2;
                                       sortby='Price: highest to low';
                                     // print("price ${pressedprice2}");
                                    });
                                    },
                                  ),
                                ),

                                // InkWell(
                                //   onTap: (() {
                                //     print("object");
                                //     setState(() {
                                //       pressed=!pressed;
                                //     });
                                    
                                //   }),
                                //   child: Padding(
                                //     padding: const EdgeInsets.all(12.0),
                                //     child: Card(
                                //       color:  pressed==true? val: val2,
                                //       child: Text(
                                //         'LogIn',
                                //         style: AppTextStyle.style,
                                //        // textAlign: TextAlign.left
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            )
                          ],
                        ))),
            
              ],
            ),
          );
        });
  }



}

