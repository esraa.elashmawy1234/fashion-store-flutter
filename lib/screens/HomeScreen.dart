// ignore_for_file: non_constant_identifier_names, file_names

import 'package:flutter/material.dart';
import 'package:flutter_fashion_store_application_/widgets/CatigoriesListWidget.dart';

import '../widgets/CardWidget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<HomeScreen> {
  List CatigoriesText = ["New Arrival", "Clothes", "Furniture", "Machinary"];
  List CatigoriesImage = [
    "images/New..png",
    "images/clothes.png",
    "images/couch.png",
    "images/machine.png"
  ];

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    //SingleChildScrollView
    return Scaffold(
      backgroundColor: const Color(0xff1E1F28),
      appBar: AppBar(
        elevation: 0,
        leading: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          // we don't use only(left: 15) since it will not work in all languages 
          child: Icon(
            Icons.arrow_back_ios,
            size: 17.45,
          ),
        ),
        title: const Center(
            child: Text("Catigories",
                style: TextStyle(
                    fontFamily: 'Metropolis',
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w400))),
        backgroundColor: const Color(0xff1E1F28),
        actions: const [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child:  Icon(
              Icons.search,
              size: 17.95,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Column(
            children: [
              CardWidget(h: h, w: w),
              CatigoriesListWidget(
                  CatigoriesImage: CatigoriesImage,
                  CatigoriesText: CatigoriesText),
            ],
          ),
        ),
      ),
    );
  }
}
