// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_fashion_store_application_/styles/colorstyles.dart';

class GriditemsWidget extends StatefulWidget {
  const GriditemsWidget({Key? key}) : super(key: key);

  @override
  State<GriditemsWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<GriditemsWidget> {
  @override
  Widget build(BuildContext context) {
        double h = MediaQuery.of(context).size.height;

    
     List<Map<String, Object>> values = [
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt SPANISH",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Dorothy Perkins",
      "productname": "Blouse",
      "price": 21,
      "fav": false,
      "discount": 20,
      "after_discount": 14,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Dorothy Perkins",
      "productname": "Blouse",
      "price": 16,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "Shirt",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "Light Blouse",
      "price": 18,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 16,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product2.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 13,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
    {
      "imageurl": "assets/images/product1.png",
      "rating": 0,
      "brandname": "Mango",
      "productname": "T-shirt",
      "price": 17,
      "fav": false,
      "discount": Null,
      "after_discount": Null,
    },
  ];

      return SizedBox(
      height: h * 0.7,
      child: GridView.builder(
          primary: false,
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 300,
              childAspectRatio: 2 / 2,
              crossAxisSpacing: 1,
              mainAxisSpacing: 40),
          itemCount: values.length,
          itemBuilder: (BuildContext ctx, index) {
            return Stack(
              children: [
                Wrap(
                  children: [
                    Center(
                      child: Stack(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: Image.network(
                                '${values[index]['imageurl']}',
                                fit: BoxFit.cover,
                              )),
                        ],
                      ),
                    ),
                    //product_details(index)
                  ],
                ),
                Positioned(
                  right: 35,
                  top: 160,
                  child: CircleAvatar(
                    backgroundColor: const Color(0xFF2A2C36),
                    maxRadius: 18,
                    child: IconButton(
                      icon: values[index]['fav'] == false
                          ? const Icon(
                              Icons.favorite_outline,
                              color: Color(0xFFDADADA),
                              size: 15,
                            )
                          : const Icon(
                              Icons.favorite,
                              color: secondaryColor,
                              size: 15,
                            ),
                      onPressed: () {
                          setState(() {
                          if (values[index]['fav'] == false) {
                            values[index]['fav'] = true;
                          } else {
                            values[index]['fav'] = false;
                          }
                          // values[index]['fav']= !values[index]['fav'];
                          });
                      },
                    ),
                  ),
                ),
                values[index]['discount'] ==Null? Container():
                Positioned(
                  right: 145,
                  top: 9,
                  child: Chip(
                    padding: const EdgeInsets.all(2),
                    backgroundColor: secondaryColor,
                    label: Text(
                      '-${values[index]['discount']}%',
                      style: AppbarTextStyle.style,
                    ), //Text
                  ),
                )
              ],
            );
          }),
    );
  
    
  }
}