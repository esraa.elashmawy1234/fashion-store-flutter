// ignore_for_file: file_names

import 'package:flutter/material.dart';


import '../styles/colorstyles.dart';

class MyWidget extends StatefulWidget {
  final double wg;
  const MyWidget({Key? key, required this.wg}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  @override
  Widget build(BuildContext context) {
    // ignore: prefer_typing_uninitialized_variables
    var w;
    return SizedBox(
      width: w,
      height: 45,
      // child: Row(
      //   children: [
      //   IconButton(onPressed: (){}, icon: const Icon(Icons.filter_list, color: Colors.white,)),
      //   Text("Filters", style: AppTextStyle.style,),

      //    IconButton(onPressed: (){}, icon: const Icon(Icons.swap_vert, color: Colors.white,)),
      //   Text("Price: lowest to high", style: AppTextStyle.style,),

      //   IconButton(onPressed: (){}, icon: const Icon(Icons.view_list, color: Colors.white,)),

      // ],),
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
        dense: true,
        leading: SizedBox(
          width: w * 0.3,
          child: Row(children: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.filter_list,
                  color: Colors.white,
                )),
            Text(
              "Filters",
              style: AppbarTextStyle.style,
            ),
          ]),
        ),
        title: SizedBox(
          width: w * 0.3,
          child: Row(children: [
            IconButton(
                onPressed: () {
                  //BottomBarWidget();
                 // newMethod();
                },
                icon: const Icon(
                  Icons.swap_vert,
                  color: Colors.white,
                )),
            Text(
              "Price: lowest to high",
              style: AppbarTextStyle.style,
            ),
          ]),
        ),
        trailing: IconButton(
            onPressed: () {
              
            },
            icon: const Icon(
              Icons.view_list,
              color: Colors.white,
            )),
      ),
    );
  }





}