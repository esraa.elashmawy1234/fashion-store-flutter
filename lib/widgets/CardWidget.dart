// ignore_for_file: file_names

import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final double? w;
  final double? h;
  const CardWidget({Key? key, this.w, this.h}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 32, 0, 28),
      child: SizedBox(
        width: w,
        height: h! * 0.15,
        child: Card(
            color: const Color(0XFFFF3E3E),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Flexible(
                    child: Text(
                  'Sales',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Metropolis',
                  ),
                )),
                SizedBox(
                  height: 4,
                ),
                Flexible(
                    child: Text(
                  'Up to 50% off',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Metropolis',
                  ),
                )),
              ],
            )),
      ),
    );
  }
}
