// ignore_for_file: file_names

import 'package:flutter/material.dart';


class ScrollOptionsWidget extends StatelessWidget {
  final double w;
  const ScrollOptionsWidget({Key? key,required this.w}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     // ignore: non_constant_identifier_names
     List Catigories = const [
    'T-shirts',
    'Crop tops',
    'Blouses',
    'Shorts',
    'Skirts',
    'Dresses',
    'Blazers',
    'Pants',
    'Jeans',
    'Sweaters',
    'Cardigans'
  ];
    return SizedBox(
      width: w,
      height: 45,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: Catigories.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Chip(
                //elevation: 20,
                padding:const EdgeInsets.all(8),
                backgroundColor: Colors.white,
                //shadowColor: Colors.black,
                label: Text(
                  Catigories[index],
                  style: const TextStyle(
                      fontSize: 14, fontWeight: FontWeight.w400),
                ), //Text
              ),
            );
          }),
    );
    
  }
}