// ignore_for_file: deprecated_member_use, file_names

import 'package:flutter/material.dart';

import 'package:flutter_fashion_store_application_/styles/colorstyles.dart';

class BottomBarWidget extends StatefulWidget {
  const BottomBarWidget({Key? key}) : super(key: key);

  @override
  State<BottomBarWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<BottomBarWidget> {
  @override
  Widget build(BuildContext context) {
    return showMenu();
    // return Scaffold(
      
     
    //   // bottomNavigationBar: BottomAppBar(
    //   //   child: Row(children: <Widget>[
    //   //     IconButton(
    //   //       onPressed: showMenu,
    //   //       icon: const Icon(Icons.menu),
    //   //       color: Colors.white,
    //   //     ),   
    //   //   ]),
    //   // ),
    // );
  }

   showMenu() {
    bool pressedpopular=false;
    bool pressednew=false;
    bool pressedprice=false;
    bool pressedprice2=false;
    bool pressedreview=false;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              color: Color(0xFF1E1F28),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                SizedBox(
                  height: 50,
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Image.asset("assets/images/Rectangle.png"),
                      const SizedBox(height: 15),
                      const Center(
                          child: Text("Sort By",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white,
                                  fontSize: 18))),
                    ],
                  ),
                ),
                SizedBox(
                    height: (56 * 4.4).toDouble(),
                    child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(16.0),
                            topRight: Radius.circular(16.0),
                          ),
                          color: Color(0xFF1E1F28),
                        ),
                        child: Stack(
                          alignment: const Alignment(0, 0),
                          // overflow: Overflow.visible,
                          children: <Widget>[
                            ListView(
                              physics: const NeverScrollableScrollPhysics(),
                              children: <Widget>[

                                Container(
                                  color: pressedpopular== true? secondaryColor: appColor,
                                  child: ListTile(
                                    title: const Text(
                                      "Popular",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        pressedpopular!=pressedpopular;
                                      });
                                      },
                                  ),
                                ),
                                ListTile(
                                  selectedColor: pressednew==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Newest",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {
                                    setState(() {
                                        pressednew!=pressednew;
                                      });
                                  },
                                ),
                                ListTile(
                                  selectedColor: pressedreview==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Customer Review",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {setState(() {
                                      pressedreview=!pressedreview;
                                    });},
                                ),
                                ListTile(
                                  selected: true,
                                  selectedColor: pressedprice==true? secondaryColor:appColor,
                                  title: const Text(
                                    "Price: lowest to high",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onTap: () {setState(() {
                                      pressedprice=!pressedprice;
                                    });},
                                ),
                                Container(
                                  color: pressedprice2? secondaryColor: appColor,
                                  child: ListTile(
                                    title: const Text(
                                      "Price: highest to low",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onTap: () { setState(() {
                                      pressedprice2=!pressedprice2;
                                     // print("price ${pressedprice2}");
                                    });
                                    },
                                  ),
                                ),

                                // InkWell(
                                //   onTap: (() {
                                //     print("object");
                                //     setState(() {
                                //       pressed=!pressed;
                                //     });
                                    
                                //   }),
                                //   child: Padding(
                                //     padding: const EdgeInsets.all(12.0),
                                //     child: Card(
                                //       color:  pressed==true? val: val2,
                                //       child: Text(
                                //         'LogIn',
                                //         style: AppTextStyle.style,
                                //        // textAlign: TextAlign.left
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            )
                          ],
                        ))),
            
              ],
            ),
          );
        });
  }
}
