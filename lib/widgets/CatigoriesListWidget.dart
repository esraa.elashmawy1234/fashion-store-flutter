// ignore_for_file: non_constant_identifier_names, file_names

import 'package:flutter/material.dart';

class CatigoriesListWidget extends StatelessWidget {
  final List? CatigoriesText;
  final List? CatigoriesImage;
  const CatigoriesListWidget(
      {Key? key, this.CatigoriesImage, this.CatigoriesText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: h*0.6,
      child: ListView.builder(
          primary: false,
          shrinkWrap: true,
          itemCount: CatigoriesText?.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: Container(
                height: 100,
                width: 343,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Color(0xFF2A2C36)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 23),
                          child: Text(
                            CatigoriesText![index],
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Metropolis',),
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: Image.asset(CatigoriesImage![index],
                            fit: BoxFit.cover)),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
