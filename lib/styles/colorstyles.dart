import 'package:flutter/material.dart';

const Color appColor =  Color(0xFF1E1F28);
const Color secondaryColor = Color(0xFFEF3651);
const Color whiteBlue = Color(0xffCCE1F2);

class AppTextStyle {
  static TextStyle style = const
      TextStyle(fontWeight: FontWeight.normal, color: Colors.white, fontSize: 16);
  //

}
class AppbarTextStyle {
  static TextStyle style = const
      TextStyle(fontWeight: FontWeight.w400, color: Colors.white, fontSize: 11);
  //
}

class App14TextStyle {
  static TextStyle style = const
      TextStyle(fontWeight: FontWeight.w400, color: Colors.white, fontSize: 14);
  //
}