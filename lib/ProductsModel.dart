// ignore_for_file: file_names

class ProductsModel{
  String imageurl;
  int rating=0;
  String brandname;
  String productname;
  int price;
  int? discountedPrice;

  ProductsModel({required this.brandname,this.discountedPrice,required this.imageurl,
  required this.price,required this.productname, required this.rating});

}